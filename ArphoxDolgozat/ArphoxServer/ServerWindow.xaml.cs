﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Xml.Linq;
using System.IO;
using System.Threading;
using Microsoft.Win32;

namespace ArphoxServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MyServer server; //szerver funkciók

        public MainWindow()
        {
            InitializeComponent();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Logger.LogServerStart();

            server = new MyServer(this);
            labelServerIP.Content = MyServer.GetLocaLIPAdressSERVERSIDE();
            labelServerPort.Content = MyServer.serverPort;

            buttonStartServer_Click(null, null);


            //if (server.VanETanuloiLista())
            //{
            //    server.LoadTanuloLista("tanulok.txt");
            //    buttonLoadStudentList.IsEnabled = false;
            //    buttonLoadStudentList.Content = "Tanulólista betöltve.";
            //}


            //Annak ellenőrzése, hogy pendrive-on fut-e a szerver
            var drives = DriveInfo.GetDrives().Where(drive => drive.IsReady && drive.DriveType == DriveType.Removable);

            foreach (var item in drives)
            {
                string pathRoot = System.IO.Path.GetPathRoot(Assembly.GetEntryAssembly().Location).ToUpper();
                if (pathRoot == item.RootDirectory.Name)
                    MessageBox.Show("Valószínűleg pendrive-ról fut az alkalmazás, helyezd át egy helyi meghajtóra!", "Javaslat", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Logger.LogServerExit();
            Application.Current.Shutdown(0);
        }

        private void buttonStartServer_Click(object sender, RoutedEventArgs e)
        {
            buttonStartServer.IsEnabled = false;

            Task.Run(() => server.StartFileListenning());
        }
        private void buttonStopServer_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void buttonAddPlaceholder_Click(object sender, RoutedEventArgs e)
        {
            string uzenet = "                       ------------------------------";
            listBox.Items.Insert(0, uzenet);
            Logger.LogUzenet(uzenet);
        }

        #region Tanulólista (OFFLINE)
        private void buttonLoadStudentList_Click(object sender, RoutedEventArgs e)
        {
            //OpenFileDialog ablak = new OpenFileDialog();
            //ablak.Filter = "Szöveges fájlok (*.txt)|*.txt";
            //ablak.Title = "Add meg a tanulók listáját!";
            //ablak.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            //if (ablak.ShowDialog() == true)
            //{
            //    server.LoadTanuloLista(ablak.FileName);
            //}

            //buttonLoadStudentList.IsEnabled = false;
            //buttonLoadStudentList.Content = "Tanulólista betöltve.";
        }
        #endregion
    }
    class MyServer
    {
        //Settings:
        public const int serverPort = 13000;
        public const string directoryToCopy = "kapott";
        public static string messageToLog = string.Empty;
        //public static List<string> tanulok;

        MainWindow mainWindow;

        public MyServer(MainWindow w)
        {
            mainWindow = w;
            //tanulok = new List<string>();
        }

        public void StartFileListenning()
        {
            TcpListener server = null;
            int randomNev = 0;
            Directory.CreateDirectory(directoryToCopy);

            try
            {
                server = new TcpListener(IPAddress.Parse("0.0.0.0"), serverPort); //így bármilyen IP-re érkező csomagot hallgatja
                server.Start();

                string uzenet = string.Format("{0}\tSzerver elindítva, vár...", DateTime.Now);
                mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Insert(0, uzenet); });
                Logger.LogUzenet(uzenet);

                byte[] RecData = new byte[8192]; //szerintem mindegy a buffer mérete
                int RecBytes;

                while (true) //listenning loop
                {
                    TcpClient client = server.AcceptTcpClient();
                    //File recieved:
                    try
                    {
                        string fajlnev = randomNev.ToString();

                        NetworkStream netstream = client.GetStream(); // Get a stream object for reading and writing

                        #region Fájl kiolvasása a streamről és mentése "fajlnev"-be
                        FileStream fs = new FileStream(fajlnev, FileMode.OpenOrCreate, FileAccess.Write);
                        while ((RecBytes = netstream.Read(RecData, 0, RecData.Length)) > 0)
                        {
                            fs.Write(RecData, 0, RecBytes);
                        }
                        fs.Close();
                        client.Close(); // Shutdown and end connection
                        #endregion

                        string tanuloNev = GetTanuloNev(fajlnev);   //Fájl átnevezése a hallgató nevére

                        string ujFajlPath = UjFajlNevMeghatarozo(tanuloNev);
                        if (VanEMarIlyenFajl(tanuloNev))
                        {
                            mainWindow.Dispatcher.Invoke(() => { mainWindow.listBoxErrors.Items.Insert(0, string.Format("{0}:\tDuplikáció: {1}", DateTime.Now, tanuloNev)); });
                        }

                        File.Move(fajlnev, ujFajlPath);

                        messageToLog = string.Format("{0}\t{1}", DateTime.Now, System.IO.Path.GetFileNameWithoutExtension(ujFajlPath));
                        mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Insert(0, messageToLog); });
                        Logger.LogUzenet(messageToLog);

                        randomNev++;
                    }
                    catch (Exception e)
                    {
                        mainWindow.Dispatcher.Invoke(() => { mainWindow.listBoxErrors.Items.Insert(0, "\t" + e.Message); });
                        messageToLog = string.Format(DateTime.Now + "\t" + e.Message);
                        Logger.LogError(messageToLog);
                    }
                }
            }
            catch (SocketException e)
            {
                messageToLog = string.Format(DateTime.Now + "\tSocketException: {0}", e);
                mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Insert(0, messageToLog); });
                Logger.LogError(messageToLog);
            }
            finally
            {
                messageToLog = "A szerver leállt (finally ág!!!).";
                mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Insert(0, messageToLog); });
                Logger.LogUzenet(messageToLog);
                server.Stop(); // Stop listening for new clients.
            }
        }
        private string UjFajlNevMeghatarozo(string tanuloNev)
        {
            string ujFajlNev = string.Format("{0}\\{1}", directoryToCopy, tanuloNev); //ennek kéne lennie

            int aktSzam = 2;
            if (File.Exists(ujFajlNev + ".cs")) //ha már van ilyen, akkor
            {
                while (File.Exists(ujFajlNev + aktSzam.ToString() + ".cs"))
                {
                    aktSzam++;
                }
                return ujFajlNev + aktSzam + ".cs";
            }
            else
            {
                return ujFajlNev + ".cs";
            }
        }
        private bool VanEMarIlyenFajl(string tanuloNev)
        {
            string ujFajlNev = string.Format("{0}\\{1}", directoryToCopy, tanuloNev); //ennek kéne lennie
            return File.Exists(ujFajlNev + ".cs");
        }
        private string GetTanuloNev(string fajlnev)
        {
            string[] sor = File.ReadAllLines(fajlnev)[0].Split('*');
            return sor[1];
        }

        public static string GetLocaLIPAdressSERVERSIDE()
        {
            IPAddress[] coll = Dns.GetHostAddresses(Dns.GetHostName());
            string ip = string.Empty;
            int ennyiLesz = coll.Count(x => x.AddressFamily == AddressFamily.InterNetwork);

            if (ennyiLesz == 1)
            {
                return coll.Where(x => x.AddressFamily == AddressFamily.InterNetwork).First().ToString();
            }
            else
            {
                foreach (IPAddress x in coll)
                {
                    string xstring = x.ToString();
                    if (x.AddressFamily == AddressFamily.InterNetwork
                        && //és privát cím (B osztályosnál nem tökéletes de idc)
                        (xstring.StartsWith("10.") || xstring.StartsWith("172.") || xstring.StartsWith("192.168."))
                        ) //IPv4
                    {
                        ip += x.ToString() + "\n";
                    }
                }
                return ip;
            }
        }

        /* Tanulólista
        public void LoadTanuloLista(string path)
        {
            tanulok = File.ReadLines(path).ToList();
        }
        public bool VanETanuloiLista()
        {
            return File.Exists("tanulok.txt");
        }
        */

        #region oldshit
        public void StartTextListenning()
        {
            TcpListener server = null;
            try
            {
                server = new TcpListener(IPAddress.Parse("127.0.0.1"), serverPort);
                server.Start();

                byte[] bytes = new byte[256]; //buffer

                while (true) //listenning loop
                {
                    mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add("Csatlakozásra vár..."); });

                    TcpClient client = server.AcceptTcpClient();
                    //Itt már a kliens csatlakozott
                    mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add("Csatlakozva!"); });

                    string data = null;

                    NetworkStream stream = client.GetStream(); // Get a stream object for reading and writing

                    int i;
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)  // Loop to receive all the data sent by the client.
                    {
                        //data = System.Text.Encoding.ASCII.GetString(bytes, 0, i); // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                        mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add(string.Format("Fogadott: {0}", data)); });

                        string valasz = "Megkaptam koszonom!";
                        //byte[] valaszBytes = System.Text.Encoding.ASCII.GetBytes(valasz);
                        byte[] valaszBytes = System.Text.Encoding.UTF8.GetBytes(valasz);

                        stream.Write(valaszBytes, 0, valaszBytes.Length); // Send back a response.
                        mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add("Elküldve: " + valasz); });
                    }

                    client.Close(); // Shutdown and end connection
                }
            }
            catch (SocketException e)
            {
                mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add(string.Format("SocketException: {0}", e)); });
            }
            finally
            {
                server.Stop(); // Stop listening for new clients.
            }
            mainWindow.Dispatcher.Invoke(() => { mainWindow.listBox.Items.Add("\nHit enter to continue..."); });
        }
        #endregion
    }
    static class Logger
    {
        public static void LogUzenet(string message)
        {
            File.AppendAllText("messages.log", message + "\r\n");
        }
        public static void LogError(string message)
        {
            File.AppendAllText("errors.log", message + "\r\n\r\n");
        }
        public static void LogServerStart()
        {
            LogUzenet("___________________________________________________________");
            LogUzenet(DateTime.Now + "\tSzerveralkalmazás elindítva.");
            LogUzenet("___________________________________________________________");
        }
        public static void LogServerExit()
        {
            LogUzenet("___________________________________________________________");
            LogUzenet(DateTime.Now + "\tSzerveralkalmazás leállítva.");
            LogUzenet("___________________________________________________________");
            LogUzenet("\r\n\r\n");
        }
    }
}